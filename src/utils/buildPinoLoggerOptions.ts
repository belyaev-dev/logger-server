import {isDev, isProd, isTest, defaultRedactPaths} from '../constants';
import {Bindings, LoggerOptions} from 'pino';
import {LogType} from '../types';

// Методы для форматирования логов в соответствии с требованиями Sage
// https://wiki.tcsbank.ru/pages/viewpage.action?pageId=367757988
const sageFormatters = {
  // заменяем дефолтный ключ hostname на inst
  bindings: (bindings: Bindings) => ({inst: bindings.hostname}),
  // переводим представление уровня лога в upper case
  level: (level: string) => ({level: level.toUpperCase()}),
};

const defaultFormatters = {
  level: (label: string, number: number) => ({level: number}),
};

const timeFormatter = () => `,"@timestamp":"${new Date(Date.now()).toISOString()}","time": ${Date.now()}`;

const getFormatters = (logFormat: LogType) => {
  // для sage нужно использовать строковое представление level, для kibana — числовое
  // https://tinkoff.slack.com/archives/CET3CPFH8/p1632906758161700
  switch (logFormat) {
    case 'sage':
      return {
        bindings: sageFormatters.bindings,
        level: sageFormatters.level,
      };
    case 'default':
    default:
      return {
        bindings: sageFormatters.bindings,
        level: defaultFormatters.level,
      };
  }
};

const buildRedactPathsFromOptions = (pinoOptions: LoggerOptions): string[] => {
  if (!pinoOptions.redact) {
    return defaultRedactPaths;
  }

  if (Array.isArray(pinoOptions.redact)) {
    return [...defaultRedactPaths, ...pinoOptions.redact];
  }

  return [...defaultRedactPaths, ...pinoOptions.redact.paths];
};

export const buildPinoLoggerOptions = (logFormat: LogType, pinoOptions: LoggerOptions = {}): LoggerOptions => {
  return {
    ...pinoOptions,
    // prettyPrint: isDev || (!isProd && !isTest),
    transport:
            isDev || (!isProd && !isTest) ? {target: 'pino-pretty'} : undefined,
    timestamp: timeFormatter,
    // пути в логе, которые нужно скрыть (содержат sensitive данные)
    redact: {
      paths: buildRedactPathsFromOptions(pinoOptions),
      censor: '[Redacted]',
      remove: false,
    },
    formatters: getFormatters(logFormat),
  };
};

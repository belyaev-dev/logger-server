import {Level} from 'pino';
import {Severity} from '@sentry/types';

/**
 * Sentry и pino имеют немного различающиеся уровни логгирования, поэтому приходится маппить тип pino на enum Sentry.
 * @param pinoLogLevel Уровень логгирования pino
 * @returns Соответствующий уровню логгирования pino уровень Sentry
 */
export function pinoToSentryLogLevel(pinoLogLevel: Level): Severity {
  switch (pinoLogLevel) {
    case 'error':
      return Severity.Error;
    case 'warn':
      return Severity.Warning;
    case 'info':
      return Severity.Info;
    case 'debug':
      return Severity.Debug;
    default:
      // Дефолтным значением является undefined, который потом резолвится в Sentry клиенте,
      // но решили для неизвестных pino level'ов показывать ошибку
      return Severity.Error;
  }
}

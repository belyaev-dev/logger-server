import {LogMessage, LogMessageRaw} from '../types';

export const normalizeLogMessage = (rawMessage: LogMessageRaw): LogMessage => {
  // в pino есть проверка на undefined в качестве значений (они обрезаются), поэтому здесь такой проверки нет
  if (typeof rawMessage !== 'object') {
    return {msg: rawMessage};
  }

  return {
    codeNs: rawMessage.codeNs,
    msg: rawMessage.message,
    err: rawMessage.error,
    payload: rawMessage.payload,
  };
};

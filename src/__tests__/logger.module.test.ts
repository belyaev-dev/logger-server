import {expectLogMessageToHaveSageKeys, extractLoggerMessage, setup} from './utils';
import {INestApplication} from '@nestjs/common';
import {SuperTest} from 'supertest';

const writeSpy = jest.spyOn(process.stdout, 'write');

describe('NestJS Logger Module', () => {
  let app: INestApplication;
  let agent: SuperTest<any>;

  beforeAll(async () => {
    const ctx = await setup();

    app = ctx.app;
    agent = ctx.agent;
  });

  beforeEach(() => {
    writeSpy.mockClear();
  });

  afterAll(async () => {
    await app.close();
  });

  it(`автоматически логирует запросы`, async () => {
    await agent.get('/test');

    const logMessage = extractLoggerMessage(writeSpy);

    expect(logMessage).toBeTruthy();
  });

  it(`автоматические логи имеют поля с запросом (req) и ответом (res)`, async () => {
    await agent.get('/test');

    const logMessage = extractLoggerMessage(writeSpy);

    expect(logMessage.req).toBeTruthy();
    expect(typeof logMessage.req).toBe('object');

    expect(logMessage.res).toBeTruthy();
    expect(typeof logMessage.res).toBe('object');
  });

  it(`автоматические логи имеют обязательные для Sage поля`, async () => {
    await agent.get('/test');

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  describe('когда в запросе присутствуют приватные данные', () => {
    it(`в query params приватные данные должны быть замаскированы`, async () => {
      await agent.get('/test?sessionId=dontstealmysessionpls');

      const logMessage = extractLoggerMessage(writeSpy);

      expect(logMessage.req.url).toBe('/test?sessionId=*');
    });

    it(`в query params не приватные данные не маскируются`, async () => {
      await agent.get('/test?token=mySecretToken&bar=baz');

      const logMessage = extractLoggerMessage(writeSpy);

      expect(logMessage.req.url).toBe('/test?token=*&bar=baz');
    });

    it(`cookie должны быть скрыты`, async () => {
      await agent.get('/test').set('Cookie', ['psId', 'mysessionagain']);

      const logMessage = extractLoggerMessage(writeSpy);

      expect(logMessage.req.headers.cookie).toBe('[Redacted]');
    });

    it(`хедер authorization должен быть скрыт`, async () => {
      await agent.get('/test').set('authorization', 'anothersession(');

      const logMessage = extractLoggerMessage(writeSpy);

      expect(logMessage.req.headers.authorization).toBe('[Redacted]');
    });
  });
});

const sentryDsn = '[TEST DSN]';

const writeSpy = jest.spyOn(process.stdout, 'write');
const mockInitFunction = jest.fn();
const mockWithScope = jest.fn();

jest.mock('@sentry/node', () => {
  return {
    init: mockInitFunction,
    withScope: mockWithScope,
  };
});

// импортируем после мокрирования сентри, чтобы при начале работы метода объект Sentry уже существовал
import {createServerLogger} from '../loggers/logger';
import {expectLogMessageToHaveSageKeys, extractLoggerMessage} from './utils';

describe('Server Logger', () => {
  const logger = createServerLogger({project: 'test', sentryDsn, env: 'dev', logType: 'sage'});

  beforeEach(() => {
    writeSpy.mockClear();
    mockWithScope.mockClear();
    mockInitFunction.mockClear();
  });

  describe('нативные опции pino', () => {
    it('пробрасываются', () => {
      const property = 'foo';
      const value = 'bar';

      const logger = createServerLogger({
        project: 'test',
        sentryDsn,
        env: 'dev',
        logType: 'sage',
        pinoOptions: {
          mixin: () => {
            return {[property]: value};
          },
        },
      });

      logger.info('whatever');

      const logMessage = extractLoggerMessage(writeSpy);
      expect(logMessage).toHaveProperty(property, value);
    });

    it('не перетирают опции из билдера', () => {
      const logger = createServerLogger({
        project: 'test',
        sentryDsn,
        env: 'dev',
        logType: 'sage',
        pinoOptions: {timestamp: false},
      });

      logger.info('whatever');

      const logMessage = extractLoggerMessage(writeSpy);
      expect(logMessage).toHaveProperty('@timestamp');
    });
  });

  it('корректно логирует строку, переданную в качестве аргумента', () => {
    logger.info('string argument message');

    const logMessage = extractLoggerMessage(writeSpy);

    expect(logMessage).toBeTruthy();
    expect(logMessage).toHaveProperty('msg');
    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('корректно логирует объект, переданный в качестве аргумента', () => {
    logger.info({message: 'object argument message'});

    const logMessage = extractLoggerMessage(writeSpy);

    expect(logMessage).toBeTruthy();
    expect(logMessage).toHaveProperty('msg');
    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет один формат лога вне зависимости от логируемого типа данных', () => {
    logger.info('string argument message');
    logger.info({message: 'object argument message'});

    const logStringMessageKeys = Object.keys(extractLoggerMessage(writeSpy, 0));
    const logObjectMessageKeys = Object.keys(extractLoggerMessage(writeSpy, 1));

    expect(logStringMessageKeys).toEqual(logObjectMessageKeys);
  });

  it('имеет все обязательные для Sage поля', () => {
    logger.info('test message');

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет обязательные для Sage поля в нужном формате', () => {
    logger.info('test message');

    const logMessage = extractLoggerMessage(writeSpy);
    const {system, group, env, dc, inst, level} = logMessage;

    expect(typeof system).toBe('string');
    expect(system.length).toBeGreaterThan(2);
    expect(system.length).toBeLessThan(512);

    expect(typeof group).toBe('string');
    expect(group.length).toBeGreaterThan(2);
    expect(group.length).toBeLessThan(512);

    expect(typeof env).toBe('string');
    expect(env.length).toBeGreaterThan(2);

    expect(typeof dc).toBe('string');
    expect(dc.length).toBeGreaterThan(2);
    expect(dc.length).toBeLessThan(5);

    expect(typeof inst).toBe('string');
    expect(inst.length).toBeLessThan(256);

    expect(typeof level).toBe('string');
    expect(level.length).toBeLessThan(5);

    expect(typeof logMessage['@timestamp']).toBe('string');
    expect(new Date(logMessage['@timestamp'])).not.toBe('Invalid date');
  });

  it('логирует под правильным уровнем (поле level)', () => {
    logger.debug('debug message');
    logger.info('info message');
    logger.warn('warn message');
    logger.error('error message');

    const debugMessage = extractLoggerMessage(writeSpy, 0);
    const infoMessage = extractLoggerMessage(writeSpy, 1);
    const warnMessage = extractLoggerMessage(writeSpy, 2);
    const errorMessage = extractLoggerMessage(writeSpy, 3);

    expect(debugMessage.level).toBe('DEBUG');
    expect(infoMessage.level).toBe('INFO');
    expect(warnMessage.level).toBe('WARN');
    expect(errorMessage.level).toBe('ERROR');
  });

  it('инициализирует Sentry при переданном параметре sentryDsn', () => {
    createServerLogger({project: 'test', sentryDsn, env: 'dev', logType: 'sage'});

    const initParams = mockInitFunction.mock.calls[0][0];

    expect(initParams).toHaveProperty('dsn');
  });

  it('отправляет отчет в Sentry при переданном свойстве useSentry = true', () => {
    const logger = createServerLogger({project: 'test', sentryDsn, env: 'dev', logType: 'sage'});

    logger.error({message: 'test error', useSentry: true});

    expect(mockWithScope).toHaveBeenCalled();
  });

  it('не отправляет отчет в Sentry при не переданном параметре useSentry', () => {
    const logger = createServerLogger({project: 'test', sentryDsn, env: 'dev', logType: 'sage'});

    logger.error({message: 'test error'});

    expect(mockWithScope).not.toHaveBeenCalled();
  });

  it('при logType === "default" выводит level числом', () => {
    const logger = createServerLogger({project: 'test', sentryDsn, env: 'dev', logType: 'default'});

    logger.debug('debug message');
    logger.info('info message');
    logger.warn('warn message');
    logger.error('error message');

    const debugMessage = extractLoggerMessage(writeSpy, 0);
    const infoMessage = extractLoggerMessage(writeSpy, 1);
    const warnMessage = extractLoggerMessage(writeSpy, 2);
    const errorMessage = extractLoggerMessage(writeSpy, 3);

    expect(debugMessage.level).toBe(20);
    expect(infoMessage.level).toBe(30);
    expect(warnMessage.level).toBe(40);
    expect(errorMessage.level).toBe(50);
  });
});

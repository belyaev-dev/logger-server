import {Test} from '@nestjs/testing';
import request, {SuperTest} from 'supertest';
import {Controller, Get, Module, INestApplication} from '@nestjs/common';
import {createNestLoggerModule, createServerLogger} from '..';
import {LogEntry} from '../types';

export const extractLoggerMessage = (processWriteSpy: jest.SpyInstance, messageNumber: number = 0): LogEntry =>
  JSON.parse(processWriteSpy.mock.calls[messageNumber][0]);

export const expectLogMessageToHaveSageKeys = (logMessage: object): void => {
  expect(logMessage).toHaveProperty('system');
  expect(logMessage).toHaveProperty('group');
  expect(logMessage).toHaveProperty('env');
  expect(logMessage).toHaveProperty('dc');
  expect(logMessage).toHaveProperty('inst');
  expect(logMessage).toHaveProperty('level');
  expect(logMessage).toHaveProperty('@timestamp');
};

type TestCtx = {
  app: INestApplication;
  agent: SuperTest<any>;
};

@Controller('test')
class TestController {
  @Get()
  async testMethod() {
    return ['test'];
  }
}

@Module({
  controllers: [TestController],
})
class TestModule {}

export async function setup(): Promise<TestCtx> {
  const logger = createServerLogger({project: 'test', sentryDsn: '', env: 'dev', logType: 'sage'});
  const LoggerModule = createNestLoggerModule(logger, {autoLogging: true});

  const moduleRef = await Test.createTestingModule({
    imports: [TestModule, LoggerModule],
  }).compile();

  const app = moduleRef.createNestApplication();
  await app.init();
  const agent = request(app.getHttpServer());

  return {app, agent};
}

import {createServerLogger} from '../loggers/logger';
import {createTypeormLogger} from '../loggers/logger.typeorm';
import {expectLogMessageToHaveSageKeys, extractLoggerMessage} from './utils';

const TEST_QUERY = 'INSERT INTO projects (name) VALUES ("COOL PROJECT")';
const TEST_PARAMETERS = ['migrationFileName'];

const writeSpy = jest.spyOn(process.stdout, 'write');

describe('Typeorm Logger', () => {
  const logger = createServerLogger({project: 'test', sentryDsn: '', env: 'dev', logType: 'sage'});
  const typeormLogger = createTypeormLogger(logger);

  beforeEach(() => {
    writeSpy.mockClear();
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logQuery ', () => {
    typeormLogger.logQuery(TEST_QUERY, TEST_PARAMETERS);

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logQuery', () => {
    typeormLogger.logQuery(TEST_QUERY, TEST_PARAMETERS);

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logQueryError', () => {
    typeormLogger.logQueryError('log query error', TEST_QUERY, TEST_PARAMETERS);

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logQuerySlow', () => {
    const QUERY_TIME = 10000;

    typeormLogger.logQuerySlow(QUERY_TIME, TEST_QUERY, TEST_PARAMETERS);

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logSchemaBuild', () => {
    typeormLogger.logSchemaBuild('Cool schema built');

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода logMigration', () => {
    typeormLogger.logMigration('Cool schema built');

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });

  it('имеет все обязательные для Sage поля в нужном формате при вызове метода log', () => {
    typeormLogger.logMigration('info');

    const logMessage = extractLoggerMessage(writeSpy);

    expectLogMessageToHaveSageKeys(logMessage);
  });
});

import {ServerResponse} from 'http';
import {Level as PinoLevel} from 'pino';
import {LoggerModule} from 'nestjs-pino';
import {DynamicModule} from '@nestjs/common';
import {isProd} from '../constants';
import {ISpecialsServerLogger} from '../types';
import {AutoLoggingOptions} from 'pino-http';

enum PinoLogLevel {
  trace = 'trace',
  debug = 'debug',
  info = 'info',
  warn = 'warn',
  error = 'error',
  fatal = 'fatal',
}

const MASKED_PARAM_STUB = '*';
const MASKED_QUERY_PARAMS = ['token', 'sessionid', 'sessionId'];

const getLogLevelByStatusCode = (statusCode: number): PinoLevel => {
  if (statusCode >= 500) {
    return PinoLogLevel.error;
  }

  if (statusCode >= 400) {
    return PinoLogLevel.warn;
  }

  return PinoLogLevel.info;
};

const hasSensitiveData = (url: string): boolean => MASKED_QUERY_PARAMS.some((param) => url.indexOf(`${param}=`) !== -1);

// маскируем приватные query-параметры
const maskSensitiveDataInUrlQuery = (url: string): string => {
  if (!hasSensitiveData(url)) {
    return url;
  }

  const [path, queryString] = url.split('?');
  const queryObj = new URLSearchParams(queryString);

  for (const key of MASKED_QUERY_PARAMS) {
    if (queryObj.has(key)) {
      queryObj.set(key, MASKED_PARAM_STUB);
    }
  }

  // toSting — метод класса URLSearchParams, им рекомендуется переводить параметры в строку для подстановки в URL
  // порядок ключей сохраняется
  // https://url.spec.whatwg.org/#interface-urlsearchparams
  return `${path}?${queryObj.toString()}`;
};

interface PinoHttpOptions {
  autoLogging: boolean | AutoLoggingOptions | undefined;
}

export function createNestLoggerModule(logger: ISpecialsServerLogger, {autoLogging}: PinoHttpOptions): DynamicModule {
  return LoggerModule.forRoot({
    pinoHttp: [
      {
        serializers: {
          req: (req) => ({...req, url: maskSensitiveDataInUrlQuery(req.url)}),
        },        
        logger: logger.getPinoLogger().child({codeNs: 'info:request'}),
        // install 'pino-pretty' package in order to use the following option
        transport:
          !isProd ? {target: 'pino-pretty'} : undefined,
        customLogLevel: (res: ServerResponse): PinoLevel => getLogLevelByStatusCode(res.statusCode),
        autoLogging,
      },
    ],
  });
}

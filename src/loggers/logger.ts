import pino, {BaseLogger as PinoLogger, Level} from 'pino';
import * as Sentry from '@sentry/node';
import {Severity} from '@sentry/types';
import {TransportClass, Transport} from '@sentry/types';
import {LogMessageRaw, ISpecialsServerLogger, LogMessage, LoggerInitialParams} from '../types';
import {buildPinoLoggerOptions} from '../utils/buildPinoLoggerOptions';
import {normalizeLogMessage} from '../utils/normalizeLogMessage';
import {pinoToSentryLogLevel} from '../utils/pinoToSentryLogLevel';

class ServerLogger implements ISpecialsServerLogger {
  private readonly pinoLogger: PinoLogger;
  private readonly sentryLogger: SentryLogger;

  constructor({project, env, sentryDsn, logType, sentryTransport, pinoOptions}: LoggerInitialParams) {
    this.pinoLogger = pino(buildPinoLoggerOptions(logType, pinoOptions)).child({
      system: project,
      group: 'SPEC',
      dc: 'GCP',
      env,
    });
    this.sentryLogger = new SentryLogger(sentryDsn, sentryTransport);
    // включаем метод debug, по-умолчанию он отключен, а минимальный уровень === info
    this.pinoLogger.level = 'debug';
  }

  protected logMessage(rawMessage: LogMessageRaw, method: Level): void {
    const logMessage = normalizeLogMessage(rawMessage);
    const useSentry = Boolean(typeof rawMessage === 'object' && rawMessage.useSentry);

    this.pinoLogger[method](logMessage);

    if (useSentry) {
      this.sentryLogger.sendLogToSentry(logMessage, pinoToSentryLogLevel(method));
    }
  }

  public debug(rawMessage: LogMessageRaw) {
    this.logMessage(rawMessage, 'debug');
  }

  public info(rawMessage: LogMessageRaw) {
    this.logMessage(rawMessage, 'info');
  }

  public warn(rawMessage: LogMessageRaw) {
    this.logMessage(rawMessage, 'warn');
  }

  public error(rawMessage: LogMessageRaw) {
    this.logMessage(rawMessage, 'error');
  }

  public getPinoLogger(): PinoLogger {
    return this.pinoLogger;
  }
}

class SentryLogger {
  private readonly sentryDsn: string = '';

  constructor(sentryDsn: string, sentryTransport?: TransportClass<Transport>) {
    if (sentryDsn) {
      this.sentryDsn = sentryDsn;

      Sentry.init({
        dsn: sentryDsn,
        beforeBreadcrumb: (breadcrumb) => this.normalizeBreadcrumb(breadcrumb),
        transport: sentryTransport,
      });
    }
  }

  // убираем query-параметры из хлебных крошек в виде http-запросов, потому что они могут содержать sensitive-данные
  private normalizeBreadcrumb(breadcrumb: Sentry.Breadcrumb): Sentry.Breadcrumb {
    if (breadcrumb.category === 'http' && breadcrumb?.data?.url) {
      const [url] = breadcrumb.data.url.split('?');

      return {
        ...breadcrumb,
        data: {
          ...breadcrumb.data,
          url,
        },
      };
    }

    return breadcrumb;
  }

  public sendLogToSentry(logMessage: LogMessage, sentryLevel?: Severity) {
    if (!this.sentryDsn) {
      return;
    }

    // JSON.stringify, потому что в Sentry не поддерживаются вложенные объекты
    // если не привести payload к строке, то получим payload: [Object]
    const payload = JSON.stringify(logMessage.payload);

    Sentry.withScope((scope) => {
      scope.setExtra('context', {...logMessage, payload});
      // captureEvent, потому что метод позволяет группировать
      // события по message, чего не делает captureException
      Sentry.captureEvent({level: sentryLevel, message: logMessage.msg});
    });
  }
}

export function createServerLogger(params: LoggerInitialParams): ISpecialsServerLogger {
  return new ServerLogger(params);
}

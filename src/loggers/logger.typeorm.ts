import {Logger as ITypeormLogger} from 'typeorm';
import {ISpecialsServerLogger} from '../types';

type TypeormLogLevel = 'log' | 'info' | 'warn';

const getLogLevelFromTypeormLevel = (typeormLevel: TypeormLogLevel): 'info' | 'warn' => {
  if (typeormLevel === 'warn') {
    return typeormLevel;
  }

  return 'info';
};

class TypeormLogger implements ITypeormLogger {
  logger: ISpecialsServerLogger;

  constructor(logger: ISpecialsServerLogger) {
    this.logger = logger;
  }

  logQuery(query: string, parameters?: any[]) {
    this.logger.info({message: 'Query log', payload: {query, parameters}, codeNs: 'info:typeorm'});
  }
  logQueryError(err: string, query: string, parameters?: any[]) {
    this.logger.error({message: 'Query error', error: err, payload: {query, parameters}, codeNs: 'error:typeorm'});
  }
  logQuerySlow(time: number, query: string, parameters?: any[]) {
    this.logger.warn({message: 'Query slow', payload: {time, query, parameters}, codeNs: 'warn:typeorm'});
  }
  logSchemaBuild(msg: string) {
    this.logger.info({message: `Schema build: ${msg}`, codeNs: 'info:typeorm'});
  }
  logMigration(msg: string) {
    this.logger.info({message: `Migration: ${msg}`, codeNs: 'info:typeorm'});
  }
  log(typeormLevel: TypeormLogLevel, message: any) {
    const level = getLogLevelFromTypeormLevel(typeormLevel);

    this.logger[level]({message, codeNs: `${level}:typeorm`});
  }
}

export function createTypeormLogger(logger: ISpecialsServerLogger): ITypeormLogger {
  return new TypeormLogger(logger);
}

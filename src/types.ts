import {Transport, TransportClass} from '@sentry/types';
import {BaseLogger as PinoLogger, LoggerOptions as PinoOptions} from 'pino';

export type Env = 'dev' | 'stage' | 'prod-gke' | 'prod-bank';

export type LogType = 'sage' | 'default';

export type LoggerInitialParams = {
  project: string;
  env: Env;
  sentryDsn: string;
  logType: LogType;
  sentryTransport?: TransportClass<Transport>;
  pinoOptions?: PinoOptions;
};

export type LogMessage = {msg: string; codeNs?: string; err?: Error | string; payload?: object};

type LogMessageRawString = string;

type LogMessageRawObject = {
  message: string;
  error?: Error | string;
  payload?: object;
  codeNs?: string;
  useSentry?: boolean;
};

export type LogMessageRaw = LogMessageRawString | LogMessageRawObject;

export interface ISpecialsServerLogger {
  debug: (message: LogMessageRaw) => void;
  info: (message: LogMessageRaw) => void;
  warn: (message: LogMessageRaw) => void;
  error: (message: LogMessageRaw) => void;
  getPinoLogger: () => PinoLogger;
}

export type LogEntry = {
  pid: number;
  // название приложения
  system: string;
  // ответственная за приложение команда
  group: string;
  env: 'dev' | 'test' | 'prod';
  // датацентр
  dc: string;
  // хост приложения
  inst: string;
  level: 'ERROR' | 'WARN' | 'INFO' | 'DEBUG';
  '@timestamp': string;
  msg: string;
  codeNs: string;
  req: {
    id: number;
    method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
    url: string;
    headers: Record<string, string>;
    payload?: any;
  };
  res: {
    statusCode: number;
    message: string;
  };
  err?: {
    message: string;
    stack: string;
  };
};

export const isProd = process.env.NODE_ENV === 'production';
export const isDev = process.env.NODE_ENV === 'development';
export const isTest = process.env.NODE_ENV === 'test';

// пути в логах, которые будут замаскированы
// из-за производительности синтаксис не поддерживает несколько wildcard-операторов (*)
// кроме того, один wildcard-оператор работает только на одном уровне вложенности (проходит по ключам одного объекта)
// https://github.com/pinojs/pino/issues/631
// https://github.com/davidmarkclements/fast-redact/issues/27
export const defaultRedactPaths = [
  'payload.*.Authorization',
  'payload.*.authorization',
  'payload.*.headers.cookie',
  'err.*.headers.authorization',
  'err.*.headers.Authorization',
  'err.*.config.headers.authorization',
  'err.*.config.headers.Authorization',
  'req.*.headers.cookie',
  'res.*.apikey',
  'req.*.apikey',
  'req.headers.cookie',
  'req.headers.authorization',
  'req.headers.Authorization',
  'res.headers.authorization',
  'res.headers.Authorization',
  'req.headers["x-sso-token"]',
  'req.headers["X-SSO-TOKEN"]',
  'res.headers["x-sso-token"]',
  'res.headers["X-SSO-TOKEN"]',
];

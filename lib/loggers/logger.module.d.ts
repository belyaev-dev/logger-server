import { DynamicModule } from '@nestjs/common';
import { ISpecialsServerLogger } from '../types';
import { AutoLoggingOptions } from 'pino-http';
interface PinoHttpOptions {
    autoLogging: boolean | AutoLoggingOptions | undefined;
}
export declare function createNestLoggerModule(logger: ISpecialsServerLogger, { autoLogging }: PinoHttpOptions): DynamicModule;
export {};

"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServerLogger = void 0;
var pino_1 = __importDefault(require("pino"));
var Sentry = __importStar(require("@sentry/node"));
var buildPinoLoggerOptions_1 = require("../utils/buildPinoLoggerOptions");
var normalizeLogMessage_1 = require("../utils/normalizeLogMessage");
var pinoToSentryLogLevel_1 = require("../utils/pinoToSentryLogLevel");
var ServerLogger = /** @class */ (function () {
    function ServerLogger(_a) {
        var project = _a.project, env = _a.env, sentryDsn = _a.sentryDsn, logType = _a.logType, sentryTransport = _a.sentryTransport, pinoOptions = _a.pinoOptions;
        this.pinoLogger = (0, pino_1.default)((0, buildPinoLoggerOptions_1.buildPinoLoggerOptions)(logType, pinoOptions)).child({
            system: project,
            group: 'SPEC',
            dc: 'GCP',
            env: env,
        });
        this.sentryLogger = new SentryLogger(sentryDsn, sentryTransport);
        // включаем метод debug, по-умолчанию он отключен, а минимальный уровень === info
        this.pinoLogger.level = 'debug';
    }
    ServerLogger.prototype.logMessage = function (rawMessage, method) {
        var logMessage = (0, normalizeLogMessage_1.normalizeLogMessage)(rawMessage);
        var useSentry = Boolean(typeof rawMessage === 'object' && rawMessage.useSentry);
        this.pinoLogger[method](logMessage);
        if (useSentry) {
            this.sentryLogger.sendLogToSentry(logMessage, (0, pinoToSentryLogLevel_1.pinoToSentryLogLevel)(method));
        }
    };
    ServerLogger.prototype.debug = function (rawMessage) {
        this.logMessage(rawMessage, 'debug');
    };
    ServerLogger.prototype.info = function (rawMessage) {
        this.logMessage(rawMessage, 'info');
    };
    ServerLogger.prototype.warn = function (rawMessage) {
        this.logMessage(rawMessage, 'warn');
    };
    ServerLogger.prototype.error = function (rawMessage) {
        this.logMessage(rawMessage, 'error');
    };
    ServerLogger.prototype.getPinoLogger = function () {
        return this.pinoLogger;
    };
    return ServerLogger;
}());
var SentryLogger = /** @class */ (function () {
    function SentryLogger(sentryDsn, sentryTransport) {
        var _this = this;
        this.sentryDsn = '';
        if (sentryDsn) {
            this.sentryDsn = sentryDsn;
            Sentry.init({
                dsn: sentryDsn,
                beforeBreadcrumb: function (breadcrumb) { return _this.normalizeBreadcrumb(breadcrumb); },
                transport: sentryTransport,
            });
        }
    }
    // убираем query-параметры из хлебных крошек в виде http-запросов, потому что они могут содержать sensitive-данные
    SentryLogger.prototype.normalizeBreadcrumb = function (breadcrumb) {
        var _a;
        if (breadcrumb.category === 'http' && ((_a = breadcrumb === null || breadcrumb === void 0 ? void 0 : breadcrumb.data) === null || _a === void 0 ? void 0 : _a.url)) {
            var url = breadcrumb.data.url.split('?')[0];
            return __assign(__assign({}, breadcrumb), { data: __assign(__assign({}, breadcrumb.data), { url: url }) });
        }
        return breadcrumb;
    };
    SentryLogger.prototype.sendLogToSentry = function (logMessage, sentryLevel) {
        if (!this.sentryDsn) {
            return;
        }
        // JSON.stringify, потому что в Sentry не поддерживаются вложенные объекты
        // если не привести payload к строке, то получим payload: [Object]
        var payload = JSON.stringify(logMessage.payload);
        Sentry.withScope(function (scope) {
            scope.setExtra('context', __assign(__assign({}, logMessage), { payload: payload }));
            // captureEvent, потому что метод позволяет группировать
            // события по message, чего не делает captureException
            Sentry.captureEvent({ level: sentryLevel, message: logMessage.msg });
        });
    };
    return SentryLogger;
}());
function createServerLogger(params) {
    return new ServerLogger(params);
}
exports.createServerLogger = createServerLogger;

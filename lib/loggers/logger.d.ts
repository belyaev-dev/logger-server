import { ISpecialsServerLogger, LoggerInitialParams } from '../types';
export declare function createServerLogger(params: LoggerInitialParams): ISpecialsServerLogger;

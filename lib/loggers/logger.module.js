"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createNestLoggerModule = void 0;
var nestjs_pino_1 = require("nestjs-pino");
var constants_1 = require("../constants");
var PinoLogLevel;
(function (PinoLogLevel) {
    PinoLogLevel["trace"] = "trace";
    PinoLogLevel["debug"] = "debug";
    PinoLogLevel["info"] = "info";
    PinoLogLevel["warn"] = "warn";
    PinoLogLevel["error"] = "error";
    PinoLogLevel["fatal"] = "fatal";
})(PinoLogLevel || (PinoLogLevel = {}));
var MASKED_PARAM_STUB = '*';
var MASKED_QUERY_PARAMS = ['token', 'sessionid', 'sessionId'];
var getLogLevelByStatusCode = function (statusCode) {
    if (statusCode >= 500) {
        return PinoLogLevel.error;
    }
    if (statusCode >= 400) {
        return PinoLogLevel.warn;
    }
    return PinoLogLevel.info;
};
var hasSensitiveData = function (url) { return MASKED_QUERY_PARAMS.some(function (param) { return url.indexOf(param + "=") !== -1; }); };
// маскируем приватные query-параметры
var maskSensitiveDataInUrlQuery = function (url) {
    if (!hasSensitiveData(url)) {
        return url;
    }
    var _a = url.split('?'), path = _a[0], queryString = _a[1];
    var queryObj = new URLSearchParams(queryString);
    for (var _i = 0, MASKED_QUERY_PARAMS_1 = MASKED_QUERY_PARAMS; _i < MASKED_QUERY_PARAMS_1.length; _i++) {
        var key = MASKED_QUERY_PARAMS_1[_i];
        if (queryObj.has(key)) {
            queryObj.set(key, MASKED_PARAM_STUB);
        }
    }
    // toSting — метод класса URLSearchParams, им рекомендуется переводить параметры в строку для подстановки в URL
    // порядок ключей сохраняется
    // https://url.spec.whatwg.org/#interface-urlsearchparams
    return path + "?" + queryObj.toString();
};
function createNestLoggerModule(logger, _a) {
    var autoLogging = _a.autoLogging;
    return nestjs_pino_1.LoggerModule.forRoot({
        pinoHttp: {
            serializers: {
                req: function (req) { return (__assign(__assign({}, req), { url: maskSensitiveDataInUrlQuery(req.url) })); },
            },
            logger: logger.getPinoLogger().child({ codeNs: 'info:request' }),
            prettyPrint: !constants_1.isProd,
            customLogLevel: function (res) { return getLogLevelByStatusCode(res.statusCode); },
            autoLogging: autoLogging,
        },
    });
}
exports.createNestLoggerModule = createNestLoggerModule;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTypeormLogger = void 0;
var getLogLevelFromTypeormLevel = function (typeormLevel) {
    if (typeormLevel === 'warn') {
        return typeormLevel;
    }
    return 'info';
};
var TypeormLogger = /** @class */ (function () {
    function TypeormLogger(logger) {
        this.logger = logger;
    }
    TypeormLogger.prototype.logQuery = function (query, parameters) {
        this.logger.info({ message: 'Query log', payload: { query: query, parameters: parameters }, codeNs: 'info:typeorm' });
    };
    TypeormLogger.prototype.logQueryError = function (err, query, parameters) {
        this.logger.error({ message: 'Query error', error: err, payload: { query: query, parameters: parameters }, codeNs: 'error:typeorm' });
    };
    TypeormLogger.prototype.logQuerySlow = function (time, query, parameters) {
        this.logger.warn({ message: 'Query slow', payload: { time: time, query: query, parameters: parameters }, codeNs: 'warn:typeorm' });
    };
    TypeormLogger.prototype.logSchemaBuild = function (msg) {
        this.logger.info({ message: "Schema build: " + msg, codeNs: 'info:typeorm' });
    };
    TypeormLogger.prototype.logMigration = function (msg) {
        this.logger.info({ message: "Migration: " + msg, codeNs: 'info:typeorm' });
    };
    TypeormLogger.prototype.log = function (typeormLevel, message) {
        var level = getLogLevelFromTypeormLevel(typeormLevel);
        this.logger[level]({ message: message, codeNs: level + ":typeorm" });
    };
    return TypeormLogger;
}());
function createTypeormLogger(logger) {
    return new TypeormLogger(logger);
}
exports.createTypeormLogger = createTypeormLogger;

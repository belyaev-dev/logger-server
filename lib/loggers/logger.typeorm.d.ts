import { Logger as ITypeormLogger } from 'typeorm';
import { ISpecialsServerLogger } from '../types';
export declare function createTypeormLogger(logger: ISpecialsServerLogger): ITypeormLogger;

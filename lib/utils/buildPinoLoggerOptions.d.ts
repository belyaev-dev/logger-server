import { LoggerOptions } from 'pino';
import { LogType } from '../types';
export declare const buildPinoLoggerOptions: (logFormat: LogType, pinoOptions?: LoggerOptions) => LoggerOptions;

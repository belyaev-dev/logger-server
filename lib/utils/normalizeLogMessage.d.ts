import { LogMessage, LogMessageRaw } from '../types';
export declare const normalizeLogMessage: (rawMessage: LogMessageRaw) => LogMessage;

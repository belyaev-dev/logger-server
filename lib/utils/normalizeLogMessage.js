"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.normalizeLogMessage = void 0;
var normalizeLogMessage = function (rawMessage) {
    // в pino есть проверка на undefined в качестве значений (они обрезаются), поэтому здесь такой проверки нет
    if (typeof rawMessage !== 'object') {
        return { msg: rawMessage };
    }
    return {
        codeNs: rawMessage.codeNs,
        msg: rawMessage.message,
        err: rawMessage.error,
        payload: rawMessage.payload,
    };
};
exports.normalizeLogMessage = normalizeLogMessage;

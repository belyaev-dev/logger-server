import { Level } from 'pino';
import { Severity } from '@sentry/types';
/**
 * Sentry и pino имеют немного различающиеся уровни логгирования, поэтому приходится маппить тип pino на enum Sentry.
 * @param pinoLogLevel Уровень логгирования pino
 * @returns Соответствующий уровню логгирования pino уровень Sentry
 */
export declare function pinoToSentryLogLevel(pinoLogLevel: Level): Severity;

"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildPinoLoggerOptions = void 0;
var constants_1 = require("../constants");
// Методы для форматирования логов в соответствии с требованиями Sage
// https://wiki.tcsbank.ru/pages/viewpage.action?pageId=367757988
var sageFormatters = {
    // заменяем дефолтный ключ hostname на inst
    bindings: function (bindings) { return ({ inst: bindings.hostname }); },
    // переводим представление уровня лога в upper case
    level: function (level) { return ({ level: level.toUpperCase() }); },
};
var defaultFormatters = {
    level: function (label, number) { return ({ level: number }); },
};
var timeFormatter = function () { return ",\"@timestamp\":\"" + new Date(Date.now()).toISOString() + "\",\"time\": " + Date.now(); };
var getFormatters = function (logFormat) {
    // для sage нужно использовать строковое представление level, для kibana — числовое
    // https://tinkoff.slack.com/archives/CET3CPFH8/p1632906758161700
    switch (logFormat) {
        case 'sage':
            return {
                bindings: sageFormatters.bindings,
                level: sageFormatters.level,
            };
        case 'default':
        default:
            return {
                bindings: sageFormatters.bindings,
                level: defaultFormatters.level,
            };
    }
};
var buildRedactPathsFromOptions = function (pinoOptions) {
    if (!pinoOptions.redact) {
        return constants_1.defaultRedactPaths;
    }
    if (Array.isArray(pinoOptions.redact)) {
        return __spreadArray(__spreadArray([], constants_1.defaultRedactPaths, true), pinoOptions.redact, true);
    }
    return __spreadArray(__spreadArray([], constants_1.defaultRedactPaths, true), pinoOptions.redact.paths, true);
};
var buildPinoLoggerOptions = function (logFormat, pinoOptions) {
    if (pinoOptions === void 0) { pinoOptions = {}; }
    return __assign(__assign({}, pinoOptions), { prettyPrint: constants_1.isDev || (!constants_1.isProd && !constants_1.isTest), timestamp: timeFormatter, 
        // пути в логе, которые нужно скрыть (содержат sensitive данные)
        redact: {
            paths: buildRedactPathsFromOptions(pinoOptions),
            censor: '[Redacted]',
            remove: false,
        }, formatters: getFormatters(logFormat) });
};
exports.buildPinoLoggerOptions = buildPinoLoggerOptions;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pinoToSentryLogLevel = void 0;
var types_1 = require("@sentry/types");
/**
 * Sentry и pino имеют немного различающиеся уровни логгирования, поэтому приходится маппить тип pino на enum Sentry.
 * @param pinoLogLevel Уровень логгирования pino
 * @returns Соответствующий уровню логгирования pino уровень Sentry
 */
function pinoToSentryLogLevel(pinoLogLevel) {
    switch (pinoLogLevel) {
        case 'error':
            return types_1.Severity.Error;
        case 'warn':
            return types_1.Severity.Warning;
        case 'info':
            return types_1.Severity.Info;
        case 'debug':
            return types_1.Severity.Debug;
        default:
            // Дефолтным значением является undefined, который потом резолвится в Sentry клиенте,
            // но решили для неизвестных pino level'ов показывать ошибку
            return types_1.Severity.Error;
    }
}
exports.pinoToSentryLogLevel = pinoToSentryLogLevel;

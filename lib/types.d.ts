import { Transport, TransportClass } from '@sentry/types';
import { BaseLogger as PinoLogger, LoggerOptions as PinoOptions } from 'pino';
export declare type Env = 'dev' | 'stage' | 'prod-gke' | 'prod-bank';
export declare type LogType = 'sage' | 'default';
export declare type LoggerInitialParams = {
    project: string;
    env: Env;
    sentryDsn: string;
    logType: LogType;
    sentryTransport?: TransportClass<Transport>;
    pinoOptions?: PinoOptions;
};
export declare type LogMessage = {
    msg: string;
    codeNs?: string;
    err?: Error | string;
    payload?: object;
};
declare type LogMessageRawString = string;
declare type LogMessageRawObject = {
    message: string;
    error?: Error | string;
    payload?: object;
    codeNs?: string;
    useSentry?: boolean;
};
export declare type LogMessageRaw = LogMessageRawString | LogMessageRawObject;
export interface ISpecialsServerLogger {
    debug: (message: LogMessageRaw) => void;
    info: (message: LogMessageRaw) => void;
    warn: (message: LogMessageRaw) => void;
    error: (message: LogMessageRaw) => void;
    getPinoLogger: () => PinoLogger;
}
export declare type LogEntry = {
    pid: number;
    system: string;
    group: string;
    env: 'dev' | 'test' | 'prod';
    dc: string;
    inst: string;
    level: 'ERROR' | 'WARN' | 'INFO' | 'DEBUG';
    '@timestamp': string;
    msg: string;
    codeNs: string;
    req: {
        id: number;
        method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';
        url: string;
        headers: Record<string, string>;
        payload?: any;
    };
    res: {
        statusCode: number;
        message: string;
    };
    err?: {
        message: string;
        stack: string;
    };
};
export {};
